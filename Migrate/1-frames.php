<?php
session_start();
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');
$collection = $dnadb->frames;
?>
<html>
    <head>
        <title>Import Frames</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?php
        $entidades = array();
        $SQL = "SELECT * FROM entidades";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        while ($arr = $rs->FetchRow()) {
            $entidades[$arr[ident]] = 'container.' . strtolower(utf8_encode(str_replace(' ', '_', $arr[grupo])));
        }
        $SQL = "select * from preguntas";
        //$SQL = "select * from preguntas LIMIT 10";
        //$SQL="select * from preguntas WHERE idtipo=12";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        echo "<h1>Importando:" . $rs->RecordCount() . " Preguntas</h1>";

        while ($arr = $rs->FetchRow()) {
            echo "<h3>Importando:$arr[idpreg]</h3>";
            $data = array();
            $data[idframe] = (int) $arr['idpreg'];
            $data[title] = utf8_encode($arr[descripcion]);
            //$data[desc] = utf8_encode($arr[descripcion]);
            $SQL = "select ident from formularios where idform=$arr[idform]";
            $rsform = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
            $data[container] = $entidades[$rsform->Fields('ident')];
//---convert type----------

            switch ($arr['idtipo']) {
                case 1: //------option List
                    $data['type'] = 'radio';
                    $data['cols'] = $arr[ancho];
                    $data[idop] = (int) $arr['idopcion'];
                    list($otros, $idrel, $orden) = explode("*", $otros);
                    $data['allowOther'] = $otros;
                    $data['idrel'] = $idrel;
                    $data['orderBy'] = $orden;
                    $data['default'] = $arr[param2];
                    break;

                case 4://------Check
                    $data['type'] = 'checklist';
                    $data['cols'] = $arr['ancho'];
                    $data[idop] = (int) $arr['idopcion'];
                    list($otros, $idrel, $orden) = explode("*", $arr['param1']);
                    $data['allowOther'] = $otros;
                    $data['idrel'] = $idrel;
                    $data['orderBy'] = $orden;
                    $data['default'] = array_filter(explode('*', $arr['param2']), 'trim');

                    break;

                case 7://-------Tabla
                    $data['type'] = 'table';
                    list($data['cols'], $data['rows']) = explode('*', $arr['param1']);
                    $labels = explode('*', $arr['param2']);
                    foreach ($labels as $label) {
                        $thislabel = '';
                        $thissize = '';
                        list($thislabel, $thissize) = explode('|', $label);
                        $data['labels'][] = array(label => utf8_encode($thislabel), size => $thissize);
                    }
                    break;

                case 2://-----Text+
                    $data['type'] = 'text';
                    $data['size'] = $arr['ancho'];
                    break;

                case 3://-----Text area
                    $data['type'] = 'textarea';
                    list($data['rows'], $data['cols']) = explode('*', $arr['param1']);
                    break;

                case 6://----rotulo
                    $data['type'] = 'label';
                    //$data[value]=$arr[descripcion];
                    break;

                case 5://---desplegable combo
                    $data['type'] = 'combo';
                    if ($arr['param3']<>'') {
                        $data['type'] = 'combodb';
                        list($collection, $frame, $state) = explode('*', $arr['param3']);
                        $frame=explode(',', $frame);
                        $SQL = "select ident from formularios inner join preguntas on preguntas.idform=formularios.idform where idpreg=".$frame[0];
                        echo $SQL;
                        $rsform = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
                        $data['fields'] = $frame;
                        $data['fieldValue'] = 'id';
                        $data['sort'] = array($frame[0] => 1);
                        $data['dataFrom'] = $entidades[$rsform->Fields('ident')];
                        $data['query'] = array(status => trim($state));
                    } else {
                        $data[idop] = (int) $arr['idopcion'];
                    }
                    list($otros, $idrel, $orden) = explode("*", $arr['param1']);
                    $data['allowOther'] = $otros;
                    $data['idrel'] = $idrel;
                    $data['orderBy'] = $orden;
                    $data['default'] = $arr[param2];
                    break;

                case 8://----Fecha
                    $data['type'] = 'date';
                    if ($arr['param2']=='FA')
                        $data['automatic'] = true;
                    break;

                case 9://----Fecha y Hora
                    $data['type'] = 'datetime';
                    if ($arr['param2']=='FA')
                        $data['automatic'] = true;
                    break;

                case 11://---Imagen
                    $data['type'] = 'file';
                    list($data['width'], $data['height']) = explode('*', $arr['param1']);
                    break;

                case 12://---Sub Formulario
                    $data['type'] = 'subform';
                    $data['cols'] = $arr['ancho'];
                    if ($arr['param2']<>'' and $arr['param2']<>0)
                        $data['mustCheck'] = true;
                    if ($arr['param2']<>'' and $arr['param2']=='0')
                        $data['mustCheck'] = false;
                    $data['object'] = str_replace('F', 'D', $arr['param1']);
                    break;

                case 13://-----firma de origen
                    $data['type'] = 'signature';
                    break;

                case 14://---Sub Formulario Padre
                    $data['type'] = 'subformparent';
                    $data['cols'] = $arr['ancho'];
                    $data['object'] = $arr['param1'];
                    break;

                case 15://------procesos
                    $data['type'] = 'process';
                    $data['basefilename'] = $arr['param1'];
                    break;

                case 16://-----Alias
                $data['type'] = 'alias';
                $paramdata=explode('*',$arr['param3']);
                $data['frameref']=$paramdata[0];
                break;
                
            }

      

            //-------------------------

            $data[section] = $arr[seccion];
            //$data[idu]        =   $arr['usuario'];
            $data[cname] = "C".$arr[idpreg];
            //$data[container]    =   $arr[tabladest];
            $data[cols] = (int) $data[cols];
            $data[rows] = (int) $data[rows];
            $data[width] = (int) $data[width];
            $data[height] = (int) $data[height];
            //----sanitize the array
            $data = array_filter($data);
            var_dump($data);
            echo "Removing $arr[idpreg] <br/>$data[type]<br/>";
            $collection = $dnadb->frames;
            $collection->remove(array(idframe => $data[idframe]), array(safe => true));
            $result = $collection->save($data, array(safe => true));
            var_dump($result);
            //$superData[]=$data;
            echo "<hr>";
        }
//$result=$collection->batchInsert($superData,array(safe=>true));
//var_dump($result);
        ?>

    </body>
</html>