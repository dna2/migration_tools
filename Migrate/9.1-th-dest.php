<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');
set_time_limit(7200);
//----Armo colecciones x Entidad

$rs= $dnadb->command(array('distinct'=>'forms','key' => 'idapp')); //--select distinct apps from FORMS
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?php
        $entidades=array();
        //-----get real tdest
        $SQL="SHOW TABLES LIKE 'td_%'";
        $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
        while($arr=$rs->FetchRow()) $real_tdest[]=$arr[0];
        
        $SQL="SELECT * FROM entidades";
        //$SQL="SELECT * FROM entidades where ident not in(4,7)";


        $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
        while($arr=$rs->FetchRow()) {
            $entidades[$arr[ident]]='history.'.strtolower(utf8_encode(str_replace(' ', '_', $arr[grupo])));
            $SQL="select distinct(tabladest) from formularios where ident=$arr[ident]";
            $rstdest=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
            if($rstdest->RecordCount()) {
                $arrtdest=array();
                while($arrt=$rstdest->FetchRow()) {
                    if(in_array($arrt[tabladest], $real_tdest)) $arrtdest[]=$arrt[tabladest];
                }
                $ent_tdest[$arr[ident]]=$arrtdest;
            }
        }


        var_dump($entidades,$ent_tdest);
        //----precargo las preguntas
        $SQL="SELECT * FROM preguntas";
        $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
        while($arr=$rs->FetchRow()) {
            $arrpreg[$arr[idpreg]]=$arr[idtipo];
        }


        echo "<h1>Importando:".count($ent_tdest)." Entidades</h1>";

        foreach ($ent_tdest as $ident=>$arrdest) {//--------para cada entidad
            echo "<h2>importando datos en:".$entidades[$ident].'</h2>';
            $collection=$dnadb->selectCollection($entidades[$ident]);
            echo "<h2>Procesando ident:$ident</h2>";

            $SQL="SELECT id,estado,idu,idg FROM idsent WHERE ident=$ident LIMIT 50";
            $SQL="SELECT id,estado,idu,idg,idparent FROM idsent WHERE ident=$ident";

            $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
            echo $rs->RecordCount().' ids<br/>';
            while($arr=$rs->FetchRow()) { //-----recorro cada id
                $data=array();
                $data[id]=(float)$arr[id];
                $data[status]=$arr[estado];
                $data[owner]=(int)$arr[idu];
                $data[group]=(int)$arr[idg];
                if($arr[idparent]<>''){
                    $parents=explode('*',$arr[idparent]);
                    $parents=array_filter($parents, 'trim');
                    $parents=array_map('floatval',$parents);
                    $data[parent]=$parents;
                }
                //----get owner
                if(!$data[owner]) {
                    $SQL="select idu from idsenthist where id=$arr[id] LIMIT 1";
                    $rso=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
                    $data[owner]=(int)$rso->Fields('idu');

                }

                //----get group
                if(!$data[group]) {
                    $SQL="select idorg from users where idusuario=$data[owner] LIMIT 1";
                    $rso=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
                    $data[group]=(int)$rso->Fields('idorg');
                }
                //----ahora recorro cada tabla para ver si tiene datos en esa tabla
                $import=false;
                foreach($arrdest as $tdest) {
                    $SQL="SELECT idpreg,valor FROM $tdest WHERE id=$arr[id] AND valor<>''";
                    $rspreg=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
                    if($rspreg->RecordCount()) {
                        while($frame=$rspreg->FetchRow()) {
                            $import=true;
                            $frame[valor]=utf8_encode($frame[valor]);
                            switch($arrpreg[$frame[idpreg]]) {

                                case 1:	//------option List
                                    $data[$frame[idpreg]]=explode('*',$frame[valor]);
                                    break;

                                case 4://------Check
                                    $data[$frame[idpreg]]=explode('*',$frame[valor]);
                                    break;

                                case 7://-------Tabla
                                    $data[$frame[idpreg]]=explode('*',$frame[valor]);
                                    break;


                                case 5://---desplegable combo
                                    $data[$frame[idpreg]]=explode('*',$frame[valor]);

                                    break;

                                case 8://----Fecha
                                    list($Y,$m,$d)=explode('/',$frame);
                                    if($Y<>'' and $m<>'' and $d<>'') $data[$frame[idpreg]]="$Y-$m-$d";
                                    break;

                                case 9://----Fecha y Hora
                                    list($Y,$m,$d,$h,$i,$s)=explode('/',$frame);
                                    if($Y<>'' and $m<>'' and $d<>'') $data[$frame[idpreg]]="$Y-$m-$d $h:$i:$s";
                                    break;


                                case 12://---Sub Formulario
                                    $data[$frame[idpreg]]=explode('*',$frame[valor]);
                                    if(count($data[$frame[idpreg]])>0)
                                    $data[$frame[idpreg]]=array_map('floatval',$data[$frame[idpreg]]);
                                    break;

                                default:
                                    $data[$frame[idpreg]]=$frame[valor];
                                    break;

                            }
                        }//-----para cada pregunta
                    }
                }


                //-------------------------------------------------------
                $data=array_filter($data);

                if($import) {
                //    var_dump($data);

                echo "Removing $data[id]<br/>";
                $collection->remove(array(id=>$data[id]),array(safe=>true));
                $result=$collection->save($data,array(safe=>true));
                //var_dump($result);
                
                    echo "<hr>";
                } //----end import

            }//----para cada id
        }

        ?>

    </body>
</html>
?>
