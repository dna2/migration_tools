<?php
session_start();
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');
$apps = $dnadb->apps;
$forms= $dnadb->forms;

$rs= $dnadb->command(array('distinct'=>'forms','key' => 'idapp')); //--select distinct apps from FORMS
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?php
        $SQL="select ident,grupo as name,iduser as idu from entidades";
        $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
        echo "<h1>Importando:".$rs->RecordCount()." Entidades</h1>";

        while($arr=$rs->FetchRow()){
            $data=array();
            $data[ident]=(int)$arr[ident];
            $data[name]=utf8_encode($arr[name]);
            $data[idu]=$arr[idu];
            $data[container]='container.' . strtolower(utf8_encode(str_replace(' ', '_', $data[name])));
            $data=array_filter($data);
            var_dump($data);

            echo "Removing $data[ident] $data[name] <br/>";
            
            $collection = $dnadb->entities;
            $collection->remove(array(ident=>$data[ident]),array(safe=>true));
            $result=$collection->save($data,array(safe=>true));
            var_dump($result);
            //$superData[]=$data;

            echo "<hr>";
        }
        ?>

    </body>
</html>
