<?php
session_start();
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');
$collection = $dnadb->forms;
$collection->remove(array(type=>'D'));
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?php
        $entidades=array();


        $SQL="SELECT * FROM entidades";
        $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
        while($arr=$rs->FetchRow()) {
            $entidades[$arr[ident]]='container.'.strtolower(utf8_encode(str_replace(' ', '_', $arr[grupo])));

        }


        $SQL="select * from formularios ";
        $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
        echo "<h1>Importando:".$rs->RecordCount()." Formularios</h1>";

        while($arr=$rs->FetchRow()) {
            echo "<h3>Importando:$arr[idform]</h3>";
            $data=array();
            //---convert type----------
            $data[type]='D'; //----D 4 Data or Definition
            //-------------------------
            $data[idform]       =   (int)$arr[idform];
            $data[idapp]        =   (int)$arr[idap];
            $data[title]        =   utf8_encode($arr[nombre]);
            $data['idobj']      =   $data[type].$arr[idform];
            $data['idu']        =   (int)$arr[iduser];
            $data['visible']    =   $arr[visible];
            $data['redir']      =   $arr[redir];
            $data['ident']      =   (int)$arr[ident];
            $data['container']  =   $entidades[$arr[ident]];

            $data['mode']    =   $arr[mode];
            //---Convert Redir
            $redir_type=$data['redir'][0];
            switch ($redir_type){
                case 'V':
                    $data['redir'][0]='E';
                    break;
                case 'I':
                    $data['redir'][0]='P';
                    break;
                case 'F':
                    $data['redir'][0]='D';
                    break;
               
            }
            

            $data['layout']      =   $arr[layout];
            $data['css']        =   $arr[estilo];

            $data[filterByUser]      =   ($arr[filterByUser]==1) ? true : '';
            $data[filterByGroup]      =   ($arr[filterByGroup]==1) ? true : '';
            //$data[idu]        =   $arr['usuario'];
            $data[cname]        =   $arr[nombrecontrol];
            $data[desc]         =   utf8_encode($arr[Descripcion]);
            $data[rows]         =   (int)$data[rows];
            $data[width]        =   (int)$data[width];
            $data[height]       =   (int)$data[height];
            //------convert filters
            //----get frames
            $SQL="SELECT * FROM preguntas WHERE idform=".$arr[idform]." ORDER BY orden";
            $rspreg=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);

            while($arrpreg=$rspreg->fetchRow()) {
                $thisarr=array();             
                //$thisarr['#']=(int)$arrpreg['orden'];
                if($arrpreg[control]=='H') $thiarr[hidden]=true;
                if($arrpreg[control]=='L') $thiarr[locked]=true;
                //$data[frames][]=(int)$arrpreg[idpreg];
                $data[frames][(int)$arrpreg[idpreg]]=$thisarr;
            }
            //----sanitize the array
            $data=array_filter($data);

            $collection = $dnadb->forms;
//            //----chequeo si existe y si existe lo re-alloc
//            $rsobj=$collection->findOne(array(idform=>$data[idform]));
//
//            if(count($rsobj)) {
//                $cursor = $collection->find()->limit(1);
//                $cursor = $cursor->sort(array("idform" => -1));
//                $f=$cursor->getNext();
//                $max=$f[idform];
//                $test=1;
//                while(true) {
//                    $rsobj=$collection->findOne(array(idform=>$test));
//                    if(!count($rsobj)) { // esta libre----
//                        $data[idform]=$test;
//                        $data[idobj]="D$test";
//                        break;
//                    } else {// ocupado!
//                        $test++;
//                    }
//                }
//
//
//                echo "<h1>REALLOCATE: $arr[idform] to $test</h1>";
//            }
            var_dump($data);

            echo "Removing $arr[idpreg] <br/>$data[type]<br/>";
            $collection = $dnadb->forms;
            $collection->remove(array(idform=>$data[idform]),array(safe=>true));
            $result=$collection->save($data,array(safe=>true));
            var_dump($result);
            //$superData[]=$data;

            echo "<hr>";
        }
        //----Formularios: reallocar formularios si colisionan con una vista

//$result=$collection->batchInsert($superData,array(safe=>true));
//var_dump($result);
        ?>

    </body>
</html>