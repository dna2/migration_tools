<?php
/* * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');
set_time_limit(5 * 3600); //---set time limito to 5 hours
//----Armo colecciones x Entidad

$rs = $dnadb->command(array('distinct' => 'forms', 'key' => 'idapp')); //--select distinct apps from FORMS
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?php
        $entidades = array();
//-----get real tdest
        $SQL = "SHOW TABLES LIKE 'td_%'";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        while ($arr = $rs->FetchRow())
            $real_tdest[] = $arr[0];
        var_dump($real_tdest);
        $SQL = "SELECT * FROM entidades";
        //---solo Empresas
        $SQL = "SELECT * FROM entidades";

        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        while ($arr = $rs->FetchRow()) {
            $entidades[$arr[ident]] = 'container.' . strtolower(utf8_encode(str_replace(' ', '_', $arr[grupo])));

            $SQL = "select distinct(tabladest) from formularios where ident=$arr[ident]";
            $rstdest = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
            if ($rstdest->RecordCount()) {
                $arrtdest = array();
                while ($arrt = $rstdest->FetchRow()) {
                    if (in_array($arrt['tabladest'], $real_tdest))
                        $arrtdest[] = $arrt[tabladest];
                }
                $ent_tdest[$arr[ident]] = $arrtdest;
            }
        }

        var_dump($entidades, $ent_tdest);
//----precargo las preguntas
        $SQL = "SELECT * FROM preguntas";
        $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
        while ($arr = $rs->FetchRow()) {
            $arrpreg[$arr[idpreg]] = $arr[idtipo];
        }


        echo "<h1>Importando:" . count($ent_tdest) . " Entidades</h1>";
        $ent_tdest1 = array();
        //$ent_tdest1=$ent_tdest;
        //--solo procesar tipo 7 (empresa)
        $ent_tdest1[7] = $ent_tdest[7];
        foreach ($ent_tdest1 as $ident => $arrdest) {//--------para cada entidad
            echo "<h2>importando datos en:" . $entidades[$ident] . '</h2>';
            $collection = $dnadb->selectCollection($entidades[$ident]);
            echo "<h2>Procesando ident:$ident</h2>";

            $SQL = "SELECT id,estado,idu,idg,idparent FROM idsent WHERE ident=$ident";
            //$SQL = "SELECT id,estado,idu,idg,idparent FROM idsent WHERE ident=$ident AND idparent<>'' AND ident=7 LIMIT 50";

            $rs = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
            echo $rs->RecordCount() . ' ids<br/>';
            while ($arr = $rs->FetchRow()) { //-----recorro cada id
                $data = array();
                $data[id] = (double) $arr[id];
                $data[status] = $arr[estado];
                $data[owner] = (int) $arr[idu];
                $data[group] = (int) $arr[idg];
                if ($arr[idparent] <> '') {
                    $parents = explode('*', $arr[idparent]);
                    $parents = array_filter($parents, 'trim');
                    $parents = array_map('floatval', $parents);

                    foreach ($parents as $thisparent) {
                        $SQL = "SELECT ident FROM idsent WHERE id=$thisparent";
                        $rsParent = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
                        if ($rsParent->RecordCount()) {
                            //       echo $thisparent.'->'.$rsParent->fields('ident').'->'.$entidades[$rsParent->fields('ident')].'<br>';
                            $data['parent'][$entidades[$rsParent->fields('ident')]][] = $thisparent;
                        }
                    }

                    //var_dump($data['parent']);
                }
                //----get owner
                if (!$data['owner']) {
                    $SQL = "select idu from idsenthist where id=$arr[id] LIMIT 1";
                    $rso = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
                    $data[owner] = (int) $rso->Fields('idu');
                }

                //----get group
                if (!$data['group']) {
                    $SQL = "select idorg from users where idusuario=$data[owner] LIMIT 1";
                    $rso = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
                    $data[group] = (int) $rso->Fields('idorg');
                }
                //----ahora recorro cada tabla para ver si tiene datos en esa tabla
                $import = false;
                foreach ($arrdest as $tdest) {
                    //echo "<h1>Processing $tdest</h2>";
                    $SQL = "SELECT idpreg,valor FROM $tdest WHERE id=$arr[id] AND valor<>''";
                    $rspreg = $forms2->Execute($SQL) or DIE($forms2->ErrorMsg() . "<br>$SQL<br>" . __FILE__ . ":line:" . __LINE__);
                    if ($rspreg->RecordCount()) {
                        while ($frame = $rspreg->FetchRow()) {
                            $import = true;
                            $frame[valor] = utf8_encode($frame[valor]);
                            $frame[idpreg] = (int) $frame[idpreg];
                            switch ($arrpreg[$frame[idpreg]]) {

                                case 1: //------option List
                                    $data[$frame[idpreg]] = (array)$frame[valor];
                                    break;

                                case 4://------Check
                                    $data[$frame[idpreg]] = array_filter(explode('*', $frame[valor]));
                                    break;

                                case 7://-------Tabla
                                    $data[$frame[idpreg]] = array_filter(explode('*', $frame[valor]));
                                    break;


                                case 5://---desplegable combo
                                    $data[$frame[idpreg]] = (array)$frame[valor];

                                    break;

                                case 8://----Fecha
                                    list($Y, $m, $d) = explode('/', $frame[valor]);
                                    if ($Y <> '' and $m <> '' and $d <> '')
                                        $data[$frame[idpreg]] = "$Y-$m-$d";
                                    break;

                                case 9://----Fecha y Hora
                                    list($Y, $m, $d, $h, $i, $s) = explode('/', $frame[valor]);
                                    if ($Y <> '' and $m <> '' and $d <> '')
                                        $data[$frame[idpreg]] = "$Y-$m-$d $h:$i:$s";
                                    break;


                                case 12://---Sub Formulario
                                    $data[$frame[idpreg]] = explode('*', $frame[valor]);
                                    $data[$frame[idpreg]] = array_map('floatval', $data[$frame[idpreg]]);
                                    break;

                                default:
                                    $data[$frame[idpreg]] = $frame[valor];
                                    break;
                            }
                        }//-----para cada pregunta
                    }
                }


                //-------------------------------------------------------
                //$data = array_map('trim', $data);
                $data = array_filter($data);

                if ($import) {
                    //echo json_encode($data) . '<br>';
                    // echo "Removing $data[id]<br/>";
                    $collection->remove(array(id => (double) $data[id]), array(safe => true));

                    //var_dump($data);
                    //echo "<hr>";
                    $result = $collection->save($data, array(safe => true));
                    //var_dump($data);
                } //----end import
            }//----para cada id
        }
        echo "<h1>FIN</h1>";
        ?>

    </body>
</html>