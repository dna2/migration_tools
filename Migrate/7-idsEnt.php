<?php
session_start();
require_once('../adodb/adodb.inc.php');
require_once('../Connections/forms2.php');
require_once('../Connections/dnadb.php');
require_once('../tools/dna2/functions.php');
require_once('cacheopciones.php');
$apps = $dnadb->apps;
$forms= $dnadb->forms;

$rs= $dnadb->command(array('distinct'=>'forms','key' => 'idapp')); //--select distinct apps from FORMS
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <?php
        $SQL="select
            id,
            estado as status,
            ident,
            fechaent as checkdate,
            server,
            mark,
            idparent,
            idu,idg from idsent";
        $rs=$forms2->Execute($SQL) or DIE ($forms2->ErrorMsg()."<br>$SQL<br>".__FILE__.":line:".__LINE__);
        echo "<h1>Importando:".$rs->RecordCount()." ids</h1>";
        
        $collection = $dnadb->idsent;
        while($arr=$rs->FetchRow()){
            $data=array();
            $data[id]=(real)$arr[id];
            $data[status]=$arr[status];
            $data[checkdate]=$arr[checkdate];
            $data[server]=$arr[server];
            $data[mark]=$arr[mark];
            $data[idparent]=explode('*',$arr[idparent]);
            $data[idu]=$arr[idu];
            $data[idg]=$arr[idg];

            $data=array_filter($data);
             
            //var_dump($data);

            echo "Removing $data[ident] $data[name] <br/>";
            
            
            $collection->remove(array(ident=>$data[id]),array(safe=>true));
            $result=$collection->save($data,array(safe=>true));
            //var_dump($result);
            //$superData[]=$data;

            echo "$data[id]<hr>";
        }
        echo "<h1>FIN</h1>";
        ?>

    </body>
</html>
