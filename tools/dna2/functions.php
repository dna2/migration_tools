<?php
function toString($val) {
    return (string)$val;
}

function toRegex(&$item,$key) {
//---usage array_walk_recursive($query,'toRegex');
    echo "$key holds $item<br/>";
    if($key=='$regex') {
        $item=new MongoRegex($item);
    }
}
function genIdFrame($conn,$collection) {
    $cursor = $collection->find()->limit(1);
    $cursor = $cursor->sort(array("idframe" => -1));
    $f=$cursor->getNext();
    return $f[idframe]+1;
}

function getRequiredStr($type) {
    return "class='required'";

}
function getvalue($id,$idframe) {
    global $dnadb;
    //var_dump('getvalue',$id,$idframe);
    $frame=$dnadb->frames->findOne(array(idframe=>$idframe),array('container'));
    $query=array(id=>$id);
    $fields=array((string)$idframe);
    if($frame[container]) {
        $result=$dnadb->selectCollection($frame[container])->findOne($query,$fields);
    } else {
        trigger_error("container property missing for: $idframe");
    }
    //var_dump($frame[container],$query,$fields,$result);
    return $result[$idframe];

}

function getOpsFromContainer($option) {
    //uses: query fields fieldRel optionFromcontainer
    global $dnadb;
    $rtnarr=array();
    if($option[fromContainer]) { // if gets data from internal db
        $query=(array)$option[query];
        $fields=$option[fieldText];
        $fields[]=$option[fieldValue];
        $fields[]=$option[fieldRel];
        $fields=array_filter($fields);
        //var_dump($query,$fields);
        $rsop=$dnadb->selectCollection($option[container])->find($query,$fields);
        while($arr=$rsop->getNext()) {
            $text=array();
            foreach($option[fieldText] as $field) $text[]=$arr[$field];
            $text=implode(' ',$text);
            $rtnarr[]=array(value=>$arr[$option[fieldValue]],text=>$text,idrel=>$arr[$option[fieldRel]]);
        }
    }
    return($rtnarr);
}
function formGetBack($idform) {
    global $dnadb;
    //---get SRC -----------------------
    $query=array('idform'=>(int)$idform);
    $formBACK=$dnadb->selectCollection('forms.back')->findOne($query);
//---get DST -----------------------
    $query=array('idform'=>(int)$idform);
    $formSRC=$dnadb->forms->findOne($query);

    $result=$dnadb->forms->save($formBACK);

    $result1=$dnadb->selectCollection('forms.back')->save($formSRC,array(safe=>true));


    return true;

//$forms2->debug=true;

}
function formCheckOut($idform) {
    global $dnadb;
    //---get DST -----------------------
    $query=array('idform'=>(int)$idform);
    $formSRC=$dnadb->forms->findOne($query);
    $result=$dnadb->selectCollection('forms.back')->save($formSRC,array(safe=>true));
    //echo "$idform -> BACK -> $result[ok]<br/>";
    return $result[ok];

//$forms2->debug=true;

}
?>
